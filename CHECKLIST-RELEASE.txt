1. Set version string in src/version.cpp

2. Set release date in installed_files/release_history.txt

3. Make a git commit (e.g. "Update version to 22.0.0")

4. Set version git tag

5. Push to the develop and master branches

6. Wait for jobs to finish OK

// =============================================================================
// Copyright 2011-2023 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef AUTO_MELEE_HPP
#define AUTO_MELEE_HPP

namespace auto_melee
{
void run();
}  // namespace auto_melee

#endif  // AUTO_MELEE_HPP
